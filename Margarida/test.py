import pytest
import dados_resultados

#################### Funcoes da Margarida ####################

num_questoes = 10

matriz_resposta = dados_resultados.dados_extracao.coleta_matriz_respostas()
respostas = dados_resultados.dados_extracao.lista_respostas(matriz_resposta)

separados = dados_resultados.dados_manipulacao.separa_respostas_alunos(respostas, num_questoes)
list_notas = dados_resultados.dados_manipulacao.coleta_lista_notas(separados)    
list_matriculas = dados_resultados.dados_manipulacao.lista_matriculas(matriz_resposta)
list_nomes = dados_resultados.dados_manipulacao.lista_nomes(matriz_resposta)
nome_matricula_nota = dados_resultados.dados_manipulacao.coleta_nome_matricula_nota(list_nomes, list_matriculas, list_notas)

lista_ordenada_por_nota = dados_resultados.coleta_lista_ordenada_por_nota(nome_matricula_nota)

#################### Funcoes auxiliares ####################

def ordenar_CSV(info_alunos, indice_ordenacao, crescente):
    lista_ordenada_nomes = sorted(info_alunos, key=lambda info_alunos: info_alunos[indice_ordenacao], reverse=crescente) 
    return lista_ordenada_nomes

# mudar  respostas de um aluno para as respostas corretas
def mudar_respostas_CSV(n_para_teste_de_fronteira):
      df = dados_resultados.dados_extracao.coleta_matriz_respostas()

      for linha in df:
            linha[5] = "a"
            linha[7] = "b"
            linha[9] = "d"
            linha[11] = "e"
            linha[13] = "b"
            linha[15] = "a"
            linha[17] = "a"
            linha[19] = "b"
            linha[21] = "c"
            linha[23] = "d"
            if(n_para_teste_de_fronteira == 0):
                  linha[5] = "f"
                  linha[7] = "f"
                  linha[9] = "f"
                  linha[11] = "f"
                  linha[13] = "f"
                  linha[15] = "f"
                  linha[17] = "f"
                  linha[19] = "f"
                  linha[21] = "f"
                  linha[23] = "f"
            if(n_para_teste_de_fronteira == 6):
                  linha[17] = "f"
                  linha[19] = "f"
                  linha[21] = "f"
                  linha[23] = "f"
            if(n_para_teste_de_fronteira == 7):
                  linha[17] = "f"
                  linha[19] = "f"
                  linha[21] = "f"
           
      return df

# mudanca de respostas para teste de fronteira
def nova_base_de_dados(qtd_alunos):
      matriz_resposta2 = mudar_respostas_CSV(qtd_alunos)
      respostas2 = dados_resultados.dados_extracao.lista_respostas(matriz_resposta2)
      separados = dados_resultados.dados_manipulacao.separa_respostas_alunos(respostas2, num_questoes)
      list_notas = dados_resultados.dados_manipulacao.coleta_lista_notas(separados)    
      list_matriculas = dados_resultados.dados_manipulacao.lista_matriculas(matriz_resposta2)
      list_nomes = dados_resultados.dados_manipulacao.lista_nomes(matriz_resposta2)
      novo_nome_matricula_nota = dados_resultados.dados_manipulacao.coleta_nome_matricula_nota(list_nomes, list_matriculas, list_notas)

      return novo_nome_matricula_nota, list_notas



########### 0 - Teste para entrada de dados ###########

# Ao iniciar o programa, ele não aceita entrada de dados como strings


########### 1 - A lista em ordem alfabética dos alunos com ###########
########### o número de matricula e a sua nota.            ###########

# teste para ordenação de nomes(lista desordenada com base nos nomes)
def test_desorde_coleta_nome_ordem_alfabetica_matricula_nota():
      assert dados_resultados.coleta_nome_ordem_alfabetica_matricula_nota(nome_matricula_nota) == [('Alice Maria', '006', 5), ('Bianca Joaquina', '004', 7), ('Carla Aparecida', '000', 7), ('Dandara Joventina', '005', 7), ('Ester Alice', '001', 6), ('Flávia Rosália', '003', 7), ('Gabriela Clementina', '002', 6)], "test sucessful"

# teste para ordenação de nomes(lista ordenada crescentemente pelos nomes) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_cresc_coleta_nome_ordem_alfabetica_matricula_nota():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 0, False)
      assert dados_resultados.coleta_nome_ordem_alfabetica_matricula_nota(novo_nome_matricula_nota) == [('Alice Maria', '006', 5), ('Bianca Joaquina', '004', 7), ('Carla Aparecida', '000', 7), ('Dandara Joventina', '005', 7), ('Ester Alice', '001', 6), ('Flávia Rosália', '003', 7), ('Gabriela Clementina', '002', 6)], "test sucessful"

# teste para ordenação de nomes(lista ordenada decrescentemente pelos nomes) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_decres_coleta_nome_ordem_alfabetica_matricula_nota():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 0, True)
      assert dados_resultados.coleta_nome_ordem_alfabetica_matricula_nota(novo_nome_matricula_nota) == [('Alice Maria', '006', 5), ('Bianca Joaquina', '004', 7), ('Carla Aparecida', '000', 7), ('Dandara Joventina', '005', 7), ('Ester Alice', '001', 6), ('Flávia Rosália', '003', 7), ('Gabriela Clementina', '002', 6)], "test sucessful"



########### 2 - A lista em ordem crescente de notas com o nome ###########
########### do aluno, o numero da matricula e a sua nota.      ###########

# teste para ordenação de notas(lista desordenada com base nas notas)
def test_desord_coleta_lista_ordenada_por_nota():
      assert dados_resultados.coleta_lista_ordenada_por_nota(nome_matricula_nota) == [('Alice Maria', '006', 5), ('Ester Alice', '001', 6), ('Gabriela Clementina', '002', 6), ('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"

# teste para ordenação de notas(lista ordenada crescentemente pelas notas) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_cresc_coleta_lista_ordenada_por_nota():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 2, False)
      assert dados_resultados.coleta_lista_ordenada_por_nota(novo_nome_matricula_nota) == [('Alice Maria', '006', 5), ('Ester Alice', '001', 6), ('Gabriela Clementina', '002', 6), ('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"

# teste para ordenação de notas(lista ordenada decrescentemente pelas notas) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_decresc_coleta_lista_ordenada_por_nota():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 2, True)
      assert dados_resultados.coleta_lista_ordenada_por_nota(novo_nome_matricula_nota) == [('Alice Maria', '006', 5), ('Ester Alice', '001', 6), ('Gabriela Clementina', '002', 6), ('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"



########### 3 - A lista em ordem crescente de notas com o nome do aluno, o numero ###########
########### da matricula e a sua nota para os alunos aprovados (>= 7.0).          ###########

#Obs: Se chamar a funcao 3 antes da 2, havera problemas, pois é utilizado uma variavel que é
# inicializada apenas na funcao 2. Isso se deve a variavel 'lista_ordenada_por_nota'.
# E na funcao 'coleta_nota_decrescente_matricula_nota_aprovados' ela nao utiliza
# essa variavel. Mas deixou a assinatura. Logo, e necessario colocar.

# teste para ordenação de notas(lista desordenada com base nas notas)
def test_desord_coleta_nota_crescente_matricula_nota_aprovados():
      assert dados_resultados.coleta_nota_crescente_matricula_nota_aprovados(nome_matricula_nota) == [('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"

# teste para ordenação de notas(lista ordenada crescentemente pelas notas) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_cresc_coleta_nota_crescente_matricula_nota_aprovados():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 2, False)
      assert dados_resultados.coleta_nota_crescente_matricula_nota_aprovados(novo_nome_matricula_nota) == [('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"

# teste para ordenação de notas(lista ordenada decrescentemente pelas notas) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_decresc_coleta_nota_crescente_matricula_nota_aprovados():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 2, True)
      assert dados_resultados.coleta_nota_crescente_matricula_nota_aprovados(novo_nome_matricula_nota) == [('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"

### Teste de Fronteiras###

# teste para valores menores que 7
def test_coleta_nota_crescente_matricula_nota_aprovados_menor_que_7():
      # lista de notas com notas menores que 7
      novo_nome_matricula_nota = nova_base_de_dados(6)[0]
      assert dados_resultados.coleta_nota_crescente_matricula_nota_aprovados(novo_nome_matricula_nota) == [], "test sucessful"


# teste para valores iguais a 7
def test_coleta_nota_crescente_matricula_nota_aprovados_igual_a_7():
      # lista de notas com notas iguais a 7
      novo_nome_matricula_nota = nova_base_de_dados(7)[0]
      assert dados_resultados.coleta_nota_crescente_matricula_nota_aprovados(novo_nome_matricula_nota) == [('Carla Aparecida', '000', 7),('Ester Alice', '001', 7),('Gabriela Clementina', '002', 7),('Flávia Rosália', '003', 7),('Bianca Joaquina', '004', 7),('Dandara Joventina', '005', 7),('Alice Maria', '006', 7)], "test sucessful"


# teste para valores maiores que 7
def test_coleta_nota_crescente_matricula_nota_aprovados_maior_que_7():
      # lista de notas com todos os valores maiores que 7
      novo_nome_matricula_nota = nova_base_de_dados(10)[0]
      assert dados_resultados.coleta_nota_crescente_matricula_nota_aprovados(novo_nome_matricula_nota) == [('Carla Aparecida', '000', 10),('Ester Alice', '001', 10),('Gabriela Clementina', '002', 10),('Flávia Rosália', '003', 10),('Bianca Joaquina', '004', 10),('Dandara Joventina', '005', 10),('Alice Maria', '006', 10)], "test sucessful"


########### 4 - A lista em ordem decrescente de notas com o nome do aluno, o numero ###########
########### da matricula e a sua nota para os alunos reprovados (< 7.0).            ###########

#Obs: Se chamar a funcao 4 antes da 2, havera problemas, pois é utilizado uma variavel que é
# inicializada na funcao 2. Isso se deve a variavel 'lista_ordenada_por_nota'.
# E na funcao 'coleta_nota_decrescente_matricula_nota_aprovados' ela nao utiliza
# essa variavel. Mas deixou a assinatura. Logo, e necessario colocar.

# teste para ordenação de notas(lista desordenada com base nas notas)
def test_desord_coleta_nota_decrescente_matricula_nota_aprovados():
      assert dados_resultados.coleta_nota_decrescente_matricula_nota_aprovados(nome_matricula_nota,lista_ordenada_por_nota) == [('Ester Alice', '001', 6), ('Gabriela Clementina', '002', 6), ('Alice Maria', '006', 5)], "test sucessful"

# teste para ordenação de notas(lista ordenada crescentemente pelas notas) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_cresc_coleta_nota_decrescente_matricula_nota_aprovados():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 2, False)
      lista_ordenada_por_nota = dados_resultados.coleta_lista_ordenada_por_nota(novo_nome_matricula_nota)
      assert dados_resultados.coleta_nota_decrescente_matricula_nota_aprovados(novo_nome_matricula_nota,lista_ordenada_por_nota) == [('Ester Alice', '001', 6), ('Gabriela Clementina', '002', 6), ('Alice Maria', '006', 5)], "test sucessful"

# teste para ordenação de notas(lista ordenada decrescentemente pelas notas) - e necessario que o .csv de repsostas esteja ordenado
def test_ordenado_decresc_coleta_nota_decrescente_matricula_nota_aprovados():
      novo_nome_matricula_nota = ordenar_CSV(nome_matricula_nota, 2, True)
      lista_ordenada_por_nota = dados_resultados.coleta_lista_ordenada_por_nota(novo_nome_matricula_nota)
      assert dados_resultados.coleta_nota_decrescente_matricula_nota_aprovados(novo_nome_matricula_nota,lista_ordenada_por_nota) == [('Ester Alice', '001', 6), ('Gabriela Clementina', '002', 6), ('Alice Maria', '006', 5)], "test sucessful"


### Teste de Fronteiras###

# teste para valores menores que 7
def test_coleta_nota_decrescente_matricula_nota_aprovados_menor_que_7():
      novo_nome_matricula_nota = nova_base_de_dados(6)[0]
      assert dados_resultados.coleta_nota_decrescente_matricula_nota_aprovados(novo_nome_matricula_nota,lista_ordenada_por_nota) == [('Carla Aparecida', '000', 6),('Ester Alice', '001', 6),('Gabriela Clementina', '002', 6),('Flávia Rosália', '003', 6),('Bianca Joaquina', '004', 6),('Dandara Joventina', '005', 6),('Alice Maria', '006', 6)], "test sucessful"

# teste para valores iguais a 7
def test_coleta_nota_decrescente_matricula_nota_aprovados_igual_a_7():
      novo_nome_matricula_nota = nova_base_de_dados(7)[0]
      assert dados_resultados.coleta_nota_decrescente_matricula_nota_aprovados(novo_nome_matricula_nota,lista_ordenada_por_nota) == [], "test sucessful"


# teste para valores maiores que 7
def test_coleta_nota_decrescente_matricula_nota_aprovados_maior_que_7():
      novo_nome_matricula_nota = nova_base_de_dados(10)[0]
      assert dados_resultados.coleta_nota_decrescente_matricula_nota_aprovados(novo_nome_matricula_nota,lista_ordenada_por_nota) == [], "test sucessful"


########### 5 - O percentual de aprovação, sabendo-se  ###########
########### que a nota mínima para aprovação é >= 7.0. ###########

def test_percentual_aprovacao():
      percentua_aprovacao = dados_resultados.coleta_percentual_aprovacao(list_notas)
      assert percentua_aprovacao == 0.5714285714285714, "test sucessful"

# teste para todos os alunos aprovados
def test_percentual_aprovacao_aprovados_com_10():
      nome_matricula_nota = nova_base_de_dados(10)[1]
      percentua_aprovacao  = dados_resultados.coleta_percentual_aprovacao(nome_matricula_nota)
      assert percentua_aprovacao == 1.0, "test sucessful"

# teste para todos os alunos reprovados
def test_percentual_aprovacao_aprovados_com_7():
      nome_matricula_nota = nova_base_de_dados(7)[1]
      percentua_aprovacao  = dados_resultados.coleta_percentual_aprovacao(nome_matricula_nota)
      assert percentua_aprovacao == 1.0, "test sucessful"

# teste para todos os alunos reprovados
def test_percentual_aprovacao_reprovados():
      nome_matricula_nota = nova_base_de_dados(6)[1]
      percentua_aprovacao  = dados_resultados.coleta_percentual_aprovacao(nome_matricula_nota)
      assert percentua_aprovacao == 0.0, "test sucessful"



########### 6. A nota que teve a maior frequência absoluta. ###########

def test_coleta_frequencia_absoluta():
      assert dados_resultados.coleta_frequencia_absoluta(list_notas) == {7: 4, 6: 2, 5: 1}, "test sucessful"

# teste para todos os alunos aprovados
def test_coleta_frequencia_absoluta_todos_aprovados_com_10():
      list_notas = nova_base_de_dados(10)[1]
      assert dados_resultados.coleta_frequencia_absoluta(list_notas) == {10: 7}, "test sucessful"

# teste para todos os alunos aprovados
def test_coleta_frequencia_absoluta_todos_aprovados_com_7():
      list_notas = nova_base_de_dados(7)[1]
      assert dados_resultados.coleta_frequencia_absoluta(list_notas) == {7: 7}, "test sucessful"

# teste para todos os alunos reprovados
def test_coleta_frequencia_absoluta_todos_reprovados():
      list_notas = nova_base_de_dados(6)[1]
      assert dados_resultados.coleta_frequencia_absoluta(list_notas) == {6: 7}, "test sucessful"


########### 7. O aluno com a maior nota (nome, matricula e nota). ###########

def test_coleta_aluno_maior_nota():
      assert dados_resultados.coleta_aluno_maior_nota(nome_matricula_nota,list_notas) == [('Carla Aparecida', '000', 7), ('Flávia Rosália', '003', 7), ('Bianca Joaquina', '004', 7), ('Dandara Joventina', '005', 7)], "test sucessful"

# teste para todos os alunos aprovados
def test_coleta_aluno_maior_nota_todos_aprovados_com_10():
      novo_nome_matricula_nota = nova_base_de_dados(10)[0]
      list_notas = nova_base_de_dados(10)[1]
      assert dados_resultados.coleta_aluno_maior_nota(novo_nome_matricula_nota,list_notas) == [('Carla Aparecida', '000', 10),('Ester Alice', '001', 10),('Gabriela Clementina', '002', 10),('Flávia Rosália', '003', 10),('Bianca Joaquina', '004', 10),('Dandara Joventina', '005', 10),('Alice Maria', '006', 10)], "test sucessful"

# teste para todos os alunos aprovados
def test_coleta_aluno_maior_nota_todos_aprovados_com_7():
      novo_nome_matricula_nota = nova_base_de_dados(7)[0]
      list_notas = nova_base_de_dados(7)[1]
      assert dados_resultados.coleta_aluno_maior_nota(novo_nome_matricula_nota,list_notas) == [('Carla Aparecida', '000', 7),('Ester Alice', '001', 7),('Gabriela Clementina', '002', 7),('Flávia Rosália', '003', 7),('Bianca Joaquina', '004', 7),('Dandara Joventina', '005', 7),('Alice Maria', '006', 7)], "test sucessful"


# teste para todos os alunos reprovados com 0
def test_coleta_aluno_maior_nota_todos_reprovados_com_0():
      novo_nome_matricula_nota = nova_base_de_dados(0)[0]
      list_notas = nova_base_de_dados(0)[1]
      assert dados_resultados.coleta_aluno_maior_nota(novo_nome_matricula_nota,list_notas) == [('Carla Aparecida', '000', 0),('Ester Alice', '001', 0),('Gabriela Clementina', '002', 0),('Flávia Rosália', '003', 0),('Bianca Joaquina', '004', 0),('Dandara Joventina', '005', 0),('Alice Maria', '006', 0)], "test sucessful"



########### 8. O aluno com a menor nota (nome, matricula e nota) ###########

def test_coleta_aluno_menor_nota():
      assert dados_resultados.coleta_aluno_menor_nota(nome_matricula_nota,list_notas) == [('Alice Maria', '006', 5)], "test sucessful"


# teste para todos os alunos aprovados
def test_coleta_aluno_menor_nota_todos_aprovados_com_10():
      novo_nome_matricula_nota = nova_base_de_dados(10)[0]
      list_notas2 = nova_base_de_dados(10)[1]
      assert dados_resultados.coleta_aluno_menor_nota(novo_nome_matricula_nota,list_notas2) == [('Carla Aparecida', '000', 10),('Ester Alice', '001', 10),('Gabriela Clementina', '002', 10),('Flávia Rosália', '003', 10),('Bianca Joaquina', '004', 10),('Dandara Joventina', '005', 10),('Alice Maria', '006', 10)], "test sucessful"

# teste para todos os alunos aprovados
def test_coleta_aluno_menor_nota_todos_aprovados_com_7():
      novo_nome_matricula_nota = nova_base_de_dados(7)[0]
      list_notas2 = nova_base_de_dados(7)[1]
      assert dados_resultados.coleta_aluno_menor_nota(novo_nome_matricula_nota,list_notas2) == [('Carla Aparecida', '000', 7),('Ester Alice', '001', 7),('Gabriela Clementina', '002', 7),('Flávia Rosália', '003', 7),('Bianca Joaquina', '004', 7),('Dandara Joventina', '005', 7),('Alice Maria', '006', 7)], "test sucessful"

# teste para todos os alunos reprovados com 0
def test_coleta_aluno_menor_nota_todos_reprovados_com_0():
      novo_nome_matricula_nota = nova_base_de_dados(0)[0]
      list_notas2 = nova_base_de_dados(0)[1]
      assert dados_resultados.coleta_aluno_menor_nota(novo_nome_matricula_nota,list_notas2) == [('Carla Aparecida', '000', 0),('Ester Alice', '001', 0),('Gabriela Clementina', '002', 0),('Flávia Rosália', '003', 0),('Bianca Joaquina', '004', 0),('Dandara Joventina', '005', 0),('Alice Maria', '006', 0)], "test sucessful"


########### 9. A media da turma. ###########

def test_coleta_media_turma():
      assert dados_resultados.coleta_media_turma(list_notas) == 6.428571428571429 , "test sucessful"

# teste para todos os alunos aprovados com 10
def test_coleta_media_turma_todos_aprovados_com_10():
      list_notas2 = nova_base_de_dados(10)[1]
      assert dados_resultados.coleta_media_turma(list_notas2) == 10.0 , "test sucessful"

# teste para todos os alunos aprovados com 7
def test_coleta_media_turma_todos_aprovados_com_7():
      list_notas2 = nova_base_de_dados(7)[1]
      assert dados_resultados.coleta_media_turma(list_notas2) == 7.0 , "test sucessful"

# teste para todos os alunos reprovados com 0
def test_coleta_media_turma_todos_reprovados_com_0():
      list_notas2 = nova_base_de_dados(0)[1]
      assert dados_resultados.coleta_media_turma(list_notas2) == 0.0 , "test sucessful"