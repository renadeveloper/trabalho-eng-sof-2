###   ![computer](https://user-images.githubusercontent.com/38790522/87855074-4f825500-c8ec-11ea-8bfb-604cd6efc3ae.png) Teste de Software

Testes para o trabalho 2 da Margarida da disciplina Introdução a Lógica de Programação.

###  ![innovation (1)](https://user-images.githubusercontent.com/38790522/87854016-024eb500-c8e5-11ea-8d88-379cc4341e51.png) Bibliotecas e tecnologias utilizadas: 
- [Python;](https://www.python.org/)
- [Pytest;](https://docs.pytest.org/en/7.2.x/)

### <img src="https://img.icons8.com/color/30/000000/command-line.png"/> Como executar os testes

A partir da sua linha de comando:

>Execute o comando abaixo para instalar o [Pytest;](https://docs.pytest.org/en/7.2.x/).

```sh
$ python -m pip install pytest
```

>Em seguida, para executar os testes

```sh
$ python -m pytest test.py 
```


Os testes serão executados automaticamente. Posteriormente, mostrará quais testes foram executados com sucesso. 
