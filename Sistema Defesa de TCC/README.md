###   ![computer](https://user-images.githubusercontent.com/38790522/87855074-4f825500-c8ec-11ea-8bfb-604cd6efc3ae.png) Teste de Software

Testes para o sistema de defesa de TCC (https://tsiw1g3.github.io/frontend/#/)

###  ![innovation (1)](https://user-images.githubusercontent.com/38790522/87854016-024eb500-c8e5-11ea-8d88-379cc4341e51.png) Bibliotecas e tecnologias utilizadas: 
- [Python;](https://www.python.org/)
- [Unittest;](https://docs.python.org/3/library/unittest.html)
- [Poetry;](https://pypi.org/project/poetry/)

### <img src="https://img.icons8.com/color/30/000000/command-line.png"/> Como executar os testes

A partir da sua linha de comando:

>Execute o comando abaixo para instalar o [Poetry;](https://pypi.org/project/poetry/).

```sh
$ pip install poetry
```

>Em seguida, para instalar as dependências

```sh
$ poetry install
```

>E para rodar os testes:

```sh
$ python apiTests.py
```


Os testes serão executados automaticamente. Posteriormente, mostrará quantos testes foram executados corretamente.
Obs: Para poder executar os testes individualmente e ter uma melhor visualização dos resultados, recomenda-se utilizar a IDE Pycharm.
