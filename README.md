###   ![computer](https://user-images.githubusercontent.com/38790522/87855074-4f825500-c8ec-11ea-8bfb-604cd6efc3ae.png) Teste de Software

**2º Trabalho de Engenharia de Software 2.**

**Tema:** Testes de Software.

**Documento de testes:** disponível [no google docs](https://docs.google.com/document/d/1g1CHIEhdC4Oak8B9Ahhkx8UbNgOpBMVZRxdSTTeFAnw/edit?usp=sharing).

**Bug tracking:** disponível como issues/incidents neste repositório.

**Testes automáticos:** disponibilizados em pastas referentes aos projetos, cada pasta contém indicações de como executar os testes.
